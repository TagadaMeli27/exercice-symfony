<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AuthorRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Context;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;

/**
 * @ORM\Entity(repositoryClass=AuthorRepository::class)
 */
#[ApiResource(
    collectionOperations: [
        'get' => ['normalization_context' => ['groups' => 'collection:get']],
        'post'
    ],
    attributes: ["pagination_enabled" => false]
)]
class Author
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    #[Groups("collection:get")]
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups("collection:get")]
    private $name;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    #[Context([DateTimeNormalizer::FORMAT_KEY => "Y-m-d"])]
    #[Groups("collection:get")]
    private $birthDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Groups("collection:get")]
    private $birthPlace;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    #[Context([DateTimeNormalizer::FORMAT_KEY => "Y-m-d"])]
    #[Groups("collection:get")]
    private $deathDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Groups("collection:get")]
    private $deathPlace;

    /**
     * @ORM\OneToMany(targetEntity=Book::class, mappedBy="author")
     */
    #[Groups("collection:get")]
    private $books;

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getBirthPlace(): ?string
    {
        return $this->birthPlace;
    }

    public function setBirthPlace(?string $birthPlace): self
    {
        $this->birthPlace = $birthPlace;

        return $this;
    }

    public function getDeathDate(): ?\DateTimeInterface
    {
        return $this->deathDate;
    }

    public function setDeathDate(?\DateTimeInterface $deathDate): self
    {
        $this->deathDate = $deathDate;

        return $this;
    }

    public function getDeathPlace(): ?string
    {
        return $this->deathPlace;
    }

    public function setDeathPlace(?string $deathPlace): self
    {
        $this->deathPlace = $deathPlace;

        return $this;
    }

    /**
     * @return Collection|Book[]
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(Book $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books[] = $book;
            $book->setAuthor($this);
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        if ($this->books->removeElement($book)) {
            // set the owning side to null (unless already changed)
            if ($book->getAuthor() === $this) {
                $book->setAuthor(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Getting age's author if alive else his age when he dead
     */
    #[Groups("collection:get")]
    public function getAge(): ?int
    {
        // If don't have his birth date => return null
        if ($this->getBirthDate() === null)
            return null;
        
        // Check if he's alive
        $lastAliveDate = $this->getDeathDate();
        if ($lastAliveDate === null)
            $lastAliveDate = new \DateTime();
        
        // Take the date diff and return the year
        $dateDiff = $this->getBirthDate()->diff($lastAliveDate);
        return $dateDiff->y;
    }
}
