<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Genre;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    /**
     * @var ObjectManager
     */
    protected $manager;

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;

        $genreFantasy = $this->createGenre('Roman');
        $authorMartin = $this->createAuthor(
            'Victor Hugo',
            \DateTime::createFromFormat('Y-m-d', '1802-02-26'),
            'Besançon',
            \DateTime::createFromFormat('Y-m-d', '1885-05-22'),
            'Paris',
        );

        // Create a book
        $book = $this->createBook("Les Misérables", \DateTime::createFromFormat('Y', '1862'), $authorMartin, $genreFantasy);

        $genreFantasy = $this->createGenre('Fantasy');
        $authorMartin = $this->createAuthor(
            'George R. R. Martin',
            \DateTime::createFromFormat('Y-m-d', '1948-09-20'),
            'Bayonne, New Jersey, États-Unis',
            null,
            null,
        );

        // Create another book
        $book = $this->createBook("Le trône de fer", \DateTime::createFromFormat('Y-m-d', '1948-09-20'), $authorMartin, $genreFantasy);

        $manager->flush();
    }

    protected function createAuthor(
        string $name,
        \DateTime $brithDate = null,
        string $brithPlace = null,
        \DateTime $deathDate = null,
        string $deathPlace = null
    ) {
        $author = new Author();
        $author->setName($name);
        $author->setBirthDate($brithDate);
        $author->setBirthPlace($brithPlace);
        $author->setDeathDate($deathDate);
        $author->setDeathPlace($deathPlace);

        $this->manager->persist($author);

        return $author;
    }

    protected function createGenre(string $name)
    {
        $genre = new Genre();
        $genre->setName($name);

        $this->manager->persist($genre);

        return $genre;
    }

    /**
     * Book creation
     */
    protected function createBook(string $name, \DateTime $pubDate, Author $author, Genre $genre): Book
    {
        $book = new Book();
        $book->setName($name);
        $book->setPublicationDate($pubDate);
        $book->setAuthor($author);
        $book->setGenre($genre);

        $this->manager->persist($book);

        return $book;
    }
}
