<?php
namespace App\Tests\Api;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Genre;

class BooksTest extends ApiTestCase
{
    public function testGetListing(): void
    {
        // Api connexion
        $response = static::createClient()->request("GET", "/api/books");

        // Check status header and status code
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame("content-type", "application/ld+json; charset=utf-8");

        // Content verification
        $this->assertJsonContains([
            "@context" => "/api/contexts/Book",
            "@id" => "/api/books",
            "@type" => "hydra:Collection",
            "hydra:member" => [
                [
                    "@type" => "Book",
                    "name" => "Les Misérables",
                    "publicationDate" => "1862-01-08",
                ], 
                [
                "@type" => "Book",
                "name" => "Le trône de fer",
                "publicationDate" => "1948-09-20",
                ]
            ]
        ]);
    }

    public function testCreateBook(): void
    {
        // Api connexion
        $iriAuthor = $this->findIriBy(Author::class, ["name" => "Victor Hugo"]);
        $iriGenre = $this->findIriBy(Genre::class, ["name" => "Roman"]);
        $response = static::createClient()->request("POST", "/api/books", [
            "json" => [
                "name" => "Notre-Dame de Paris",
                "publicationDate" => "1831-03-16",
                "author" => $iriAuthor,
                "genre" => $iriGenre
            ]
        ]);

        // Check status header and status code
        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame("content-type", "application/ld+json; charset=utf-8");

        // Content verification
        $this->assertJsonContains([
            "@context" => "/api/contexts/Book",
            "@type" => "Book",
            "name" => "Notre-Dame de Paris",
            "publicationDate" => "1831-03-16",
            "author" => $iriAuthor,
            "genre" => $iriGenre
        ]);
    }

    public function testUpdateBook(): void
    {
        // Api connexion
        $iri = $this->findIriBy(Book::class, ["name" => "Notre-Dame de Paris"]);
        $response = static::createClient()->request("PUT", $iri, [
            "json" => [
                "name" => "Updated name"
            ]
        ]);

        // Check status code
        $this->assertResponseIsSuccessful();

        // Content verification
        $this->assertJsonContains([
            "@context" => "/api/contexts/Book",
            "@id" => $iri,
            "@type" => "Book",
            "name" => "Updated name"
        ]);
    }

    public function testDeleteBook(): void
    {
        // Api connexion
        $iri = $this->findIriBy(Book::class, ["name" => "Updated name"]);
        $response = static::createClient()->request("DELETE", $iri);

        // Check status code
        $this->assertResponseStatusCodeSame(204);
    }
}